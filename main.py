import csv ## estensione della libreria standard che ci consente di leggere file .csv
import random
import string

film_list = [] ## dove inserirò i film prodotti in Italia

with open("netflix_titles.csv", encoding="utf8") as csvfile:
    reader = csv.DictReader(csvfile) ## rendiamo il csv un dictionary iterabile da un ciclo for
    for row in reader:
        countries = row['country'].split(", ") ## andiamo a inserire in una var tutti i Paesi sotto forma di lista, usando come divisore `, + spazio`
        if 'Italy' in countries:
            film_list.append(row['title'])



#### ALGORITMO DI GIOCO
letters = [] ## lista delle lettere che il giocatore ha scelto nei suoi tentativi
attempts = 5 ## numero di tentativi dell'utente
guessed = "" ## le lettere indovinate dal giocatore

random.seed(a = 0) ## in questo modo la funzione randrange() ci restituirà sempre lo stesso risultato per finalità di debugging

film_title = film_list[random.randrange(0, len(film_list))].lower() ## il titolo del film che il giocatore dovrà indovinare

def init_word(plaintext): ## la funzione accetterà una parola in ingresso
    w = ""
    for char in plaintext: ## per ogni carattere nella parola, mostra un trattino se il carattere è una lettera, altrimenti mostra il suo valore
        if char.isspace() or char in string.punctuation:
            w += char
        else:
            w += "-"

    return w

guessed = init_word(film_title)

def check_letter(letter, word): ## la funzione accetterà la singola lettera e una parola in ingresso
    found = False
    
    for i in word:  ## se nel ciclo la lettera verrà trovata all'interno della parola, il flag found diventerà true e bloccherà il processo
        if letter == i:
            found = True
            break
    
    return found

def find_occurrencies(letter, word): ## serve a capire in che posizione del titolo è la lettera scelta dal giocatore
    occurrencies = []
    for i in range(0, len(word)):
        if letter == word[i]:
            occurrencies.append(i)
    
    return occurrencies

def sub_letters(letter, position, word): ## serve a sostituire le lettere nascoste con quelle scelte dal giocatore
    letters_list = list(word)
    letters_list[position] = letter
    return "".join(letters_list) ## crea una stringa da una lista, senza nessun carattere in mezzo

#### TURNAZIONE DEL GIOCO
game = True

while game:
    print(f"Prova a indovinare il film!\n Il film misterioso è {guessed}")
    print(f"Hai ancora {attempts} tentativi")
    
    choice = input(f"Indovina qual è il film (0 per uscire): ")

    if choice == "0":
        print(f"Grazie per aver giocato!")
        game = False
    elif choice.lower() == film_title:
        print(f"Bravissimo! Hai indovinato!")
        game = False
    else:
        print(f"Non hai indovinato!")
        print(f"Lettere già estratte: {letters}")
        choice = input(f"Estrai una lettera: ")
        choice = choice.lower()
        if check_letter(choice, letters): ## se la lettera non è stata scelta in precedenza, controlliamo che sia presente all'interno del titolo del film 
            print(f"Attenzione! Lettera già scelta.")
        else:
            letters.append(choice)
            if check_letter(choice, film_title):
                occ = find_occurrencies(choice, film_title)
                for position in occ:
                    guessed = sub_letters(choice, position, guessed)
            else:
                print(f"Lettera non presente!")
                attempts -= 1
                if attempts <= 0:
                    print(f"Hai esaurito i tentativi!")
                    print(f"Il film era: {film_title}")
                    game = False